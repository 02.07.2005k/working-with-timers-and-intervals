"use strict"

// Теоретичні питання
// 1) setTimeout: Виконує функцію один раз через вказаний проміжок часу.
// setInterval: Виконує функцію повторно через вказані інтервали часу.

// 2) Ні, реальний час виконання може бути більшим через завантаженість потоку або обмежену точність таймерів.

// 3) Для setTimeout: Використовуйте clearTimeout(timeoutId).
// Для setInterval: Використовуйте clearInterval(intervalId).

// Практичне завдання 1

document.getElementById('Button').addEventListener('click', function() {
    setTimeout(function() {
        document.getElementById('Div').textContent = 'Текст змінився )';
    }, 3000); 
});

// Практичне завдання 2

document.addEventListener('DOMContentLoaded', function() {
    let countdownValue = 10;
    const countdownElement = document.getElementById('countdown');

    const intervalId = setInterval(function() {
        countdownElement.textContent = countdownValue;
        countdownValue--;

        if (countdownValue < 1) {
            clearInterval(intervalId);
            countdownElement.textContent = 'Зворотній відлік завершено';
        }
    }, 1000);
});